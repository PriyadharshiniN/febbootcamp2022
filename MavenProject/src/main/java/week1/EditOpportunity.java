package week1;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class EditOpportunity {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		driver.get("https://login.salesforce.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofMillis(6000));
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.xpath("//input[@id = 'password']")).sendKeys("BootcampSel$123");
        driver.findElement(By.xpath("//input[@id ='Login']")).click();
        driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
        driver.findElement(By.xpath("//button[text()='View All']")).click();
        driver.findElement(By.xpath("//p[@title ='Manage your sales process with accounts, leads, opportunities, and more']")).click();  
        WebElement opp =  driver.findElement(By.xpath("//a[@title='Opportunities']"));
        driver.executeScript("arguments[0].click();", opp);
        driver.findElement(By.xpath("//a[@title='Salesforce Automation by Priya333']")).click();
        WebElement icon =driver.findElement(By.xpath("//span[text()='Show more actions']"));
        JavascriptExecutor executor3 = (JavascriptExecutor)driver;
        executor3.executeScript("arguments[0].click();", icon);
        WebElement edit = driver.findElement(By.xpath("//span[text()='Edit']"));
        JavascriptExecutor executor4 = (JavascriptExecutor)driver;
        executor4.executeScript("arguments[0].click();", edit);
        driver.findElement(By.xpath("//input[@name='CloseDate']")).clear();
        driver.findElement(By.xpath("//input[@name='CloseDate']")).sendKeys("2/28/2022");
        WebElement stage =  driver.findElement(By.xpath("//label[text()='Stage']/following-sibling::div[1]//button"));
        JavascriptExecutor executor2 = (JavascriptExecutor)driver;
        executor2.executeScript("arguments[0].click();", stage);
        WebElement stageopt = driver.findElement(By.xpath("//span[@title='Perception Analysis']"));
        JavascriptExecutor executor5 = (JavascriptExecutor)driver;
        executor5.executeScript("arguments[0].click();", stageopt);
        WebElement install = driver.findElement(By.xpath("//label[text()='Delivery/Installation Status']//following-sibling::div[1]//button"));
       JavascriptExecutor executor6 = (JavascriptExecutor)driver;
        executor6.executeScript("arguments[0].click();", install);
       WebElement installopt= driver.findElement(By.xpath("//span[@title='In progress']"));
       JavascriptExecutor executor7 = (JavascriptExecutor)driver;
       executor7.executeScript("arguments[0].click();", installopt);
       driver.findElement(By.xpath("//textarea[@class='slds-textarea']")).sendKeys("Edited");
       WebElement save =driver.findElement(By.xpath("//button[@name='SaveEdit']"));
       JavascriptExecutor executor8 = (JavascriptExecutor)driver;
       executor8.executeScript("arguments[0].click();", save);
      //Click on Details
       WebElement details = driver.findElement(By.xpath("//a[text()=\"Details\"]"));
       JavascriptExecutor executor9 = (JavascriptExecutor)driver;
       executor9.executeScript("arguments[0].click();", details);
       //verify the stage name
		WebElement name = driver.findElement(By.xpath("//span[text()='Stage']//following::lightning-formatted-text[1]"));
       System.out.println(name.getText());
	}

}
