package week1;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DeleteOpportunity {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		driver.get("https://login.salesforce.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofMillis(6000));
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.xpath("//input[@id = 'password']")).sendKeys("BootcampSel$123");
        driver.findElement(By.xpath("//input[@id ='Login']")).click();
        driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
        driver.findElement(By.xpath("//button[text()='View All']")).click();
        driver.findElement(By.xpath("//p[@title ='Manage your sales process with accounts, leads, opportunities, and more']")).click();  
        WebElement opp =  driver.findElement(By.xpath("//a[@title='Opportunities']"));
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        executor.executeScript("arguments[0].click();", opp);
        driver.findElement(By.xpath("//a[@title='Salesfoce Automation By PriyaN']")).click();
       // Click the icon to select delete
        WebElement icon =driver.findElement(By.xpath("//span[text()='Show more actions']"));
        JavascriptExecutor executor3 = (JavascriptExecutor)driver;
        executor3.executeScript("arguments[0].click();", icon);
       // select delete
        WebElement delete = driver.findElement(By.xpath("//span[text()='Delete']"));
        JavascriptExecutor executor4 = (JavascriptExecutor)driver;
        executor4.executeScript("arguments[0].click();", delete);
        WebElement delbutton = driver.findElement(By.xpath("//button[@title='Delete']"));
        JavascriptExecutor executor5 = (JavascriptExecutor)driver;
        executor5.executeScript("arguments[0].click();", delbutton);
        executor.executeScript("arguments[0].click();", opp);
//        List<WebElement> ele = driver.findElements(By.xpath("//a[@title='Salesforce Automation by PriyaN']"));
//        if (ele.size() == 0) {
//        	System.out.println("Opportunity is deleted");
//        }
//        else {
//        	System.out.println("Opportunity is not deleted");	
//        }
        driver.findElement(By.xpath("//input[@name='Opportunity-search-input']")).sendKeys("Salesforce Automation by PriyaN");
        WebElement ans = driver.findElement(By.xpath("//span[text()='No items to display.']"));
        System.out.println(ans.isDisplayed());
        
	}

}

