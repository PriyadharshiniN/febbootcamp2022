package week2;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class CreateContact {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		driver.get("https://login.salesforce.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofMillis(6000));
        
		//Login to the application
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.xpath("//input[@id = 'password']")).sendKeys("BootcampSel$123");
        driver.findElement(By.xpath("//input[@id ='Login']")).click();
        
        //Click on Global Actions SVG icon
        driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
        driver.findElement(By.xpath("//button[text()='View All']")).click();
        
        // Click on search box and enter contacts
        driver.findElement(By.xpath("//input[@placeholder='Search apps or items...']")).sendKeys("Contacts");
        JavascriptExecutor executor = (JavascriptExecutor)driver;
        WebElement el1 = driver.findElement(By.xpath("//span[@class='slds-truncate label-display']"));
        executor.executeScript("arguments[0].click();", el1);
       
        //Click on New Option
        WebElement el2 = driver.findElement(By.xpath("//div[@class='slds-grid']/child::div[2]/ul/li/a[@title='New']"));
        executor.executeScript("arguments[0].click();", el2);
        
        //Create a new account for account name
        driver.findElement(By.xpath("//label[text()='Account Name']//following::div/div/lightning-base-combobox/div/div/input[@placeholder='Search Accounts...']")).click();
        
        //select New account
        driver.findElement(By.xpath("//span[@title='New Account']")).click();
        
        //Enter Account name
        driver.findElement(By.xpath("//span[text()='Parent Account']/preceding::input[2]")).sendKeys("credit");
        
        //Click Save
        driver.findElement(By.xpath("//span[text()='Save & New']/following::button")).click();
        @SuppressWarnings("deprecation")
		WebDriverWait wait = new WebDriverWait(driver,5);
 	    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='slds-theme--success slds-notify--toast slds-notify slds-notify--toast forceToastMessage']")));
 	    String accMessage = driver.findElement(By.xpath("//div[@class='slds-theme--success slds-notify--toast slds-notify slds-notify--toast forceToastMessage']")).getText();
        System.out.println(accMessage);
        String accVerify = (accMessage.contains("credit")) ? "Account created" :"Account not created";
        System.out.println(accVerify);
 	    driver.findElement(By.xpath("//button[@class='slds-button slds-button_icon toastClose slds-notify__close slds-button--icon-inverse slds-button_icon-bare']")).click();
        
        //Click on Salutation input
        WebElement el3 =driver.findElement(By.xpath("//label[text()='Salutation']/following-sibling::div/lightning-base-combobox/div"));
        executor.executeScript("arguments[0].click();", el3);
        
        //Select Salutation as Mrs.
        WebElement el4 =driver.findElement(By.xpath("//label[text()='Salutation']/following-sibling::div/lightning-base-combobox/div/child::div[2]/lightning-base-combobox-item[4]"));
        executor.executeScript("arguments[0].click();", el4);
        
        //Enter First Name
       driver.findElement(By.xpath("//label[text()='First Name']/following-sibling::div/input[@name='firstName']")).sendKeys("PriyaD");
        
       //Enter Last Name
       driver.findElement(By.xpath("//input[@name='lastName']")).sendKeys("MuraliSS");
       
       //Enter email id
       driver.findElement(By.xpath("//input[@name='Email']")).sendKeys("priyas@test.com");
       
       //Click Save account
       driver.findElement(By.xpath("//button[@name='SaveEdit']")).click();
       
       //Get the toast message
	   wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@class='slds-theme--success slds-notify--toast slds-notify slds-notify--toast forceToastMessage']")));
       String message = driver.findElement(By.xpath("//div[@class='slds-theme--success slds-notify--toast slds-notify slds-notify--toast forceToastMessage']")).getText();
       if(message.contains("MuraliSS")) {
    	   System.out.println("Account created successfully");
       }
       else {
    	   System.out.println("Account not created successfully");
       }
	}   

}
