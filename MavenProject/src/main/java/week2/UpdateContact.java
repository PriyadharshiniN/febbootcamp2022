package week2;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class UpdateContact {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		driver.get("https://login.salesforce.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofMillis(6000));
        
		//Login to the application
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.xpath("//input[@id = 'password']")).sendKeys("BootcampSel$123");
        driver.findElement(By.xpath("//input[@id ='Login']")).click();
        
        //Click on Global Actions SVG icon
        driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
        driver.findElement(By.xpath("//button[text()='View All']")).click();
        
        //Scroll down till the element is found
        JavascriptExecutor js = (JavascriptExecutor) driver;
        WebElement contact  = driver.findElement(By.xpath("//p[text()='Contacts']"));
        js.executeScript("arguments[0].scrollIntoView();", contact);
        
        //Click on contacts
        contact.click();
        
        // Get the size of contacts available and print the list
        List<WebElement> rowcount = driver.findElements(By.xpath("//table[@class='slds-table forceRecordLayout slds-table--header-fixed slds-table--edit slds-table--bordered resizable-cols slds-table--resizable-cols uiVirtualDataTable']/tbody/tr"));
        System.out.println("Total number of contacts created :=>"+rowcount.size());
        
        //Search the contact using unique name
        driver.findElement(By.xpath("//input[@placeholder='Search this list...']")).sendKeys("MuraliSS");
        driver.findElement(By.xpath("//input[@placeholder='Search this list...']")).sendKeys(Keys.ENTER);
        Thread.sleep(5000);
        String name = "PriyaD MuraliSS";
        driver.findElement(By.xpath("//table[@class='slds-table forceRecordLayout slds-table--header-fixed slds-table--edit slds-table--bordered resizable-cols slds-table--resizable-cols uiVirtualDataTable']/tbody/tr/th/span/a[@title='"+name+"']")).click();
	    
        //Click on the dropdown icon available in the unique contact and select edit
        driver.findElement(By.xpath("//button[@class='slds-button slds-button_icon-border-filled']")).click();
        driver.findElement(By.xpath("//a[@name='Edit']")).click();
       
        //Update Email with your personal mail id
        driver.findElement(By.xpath("//input[@name='Email']")).clear();
        String upatedemail = "priyamuraliss@test.com";
        driver.findElement(By.xpath("//input[@name='Email']")).sendKeys(upatedemail);
        
        //Update Lead Source as Partner Referral from bottom
        WebElement leadSource = driver.findElement(By.xpath("//label[text()='Lead Source']/following::button[@class='slds-combobox__input slds-input_faux slds-combobox__input-value'][1]"));
        js.executeScript("arguments[0].scrollIntoView();", leadSource);	
        leadSource.click();
        driver.findElement(By.xpath("//span[@title='Partner Referral']")).click();
        
        // Update MailingAddress with personal address
        driver.findElement(By.xpath("//label[text()='Mailing City']/preceding::textarea[@class='slds-textarea']")).sendKeys("4th Street");
        driver.findElement(By.xpath("//label[text()='Mailing City']/following::input[@name='city'][1]")).sendKeys("Chennai greater area");
        driver.findElement(By.xpath("//label[text()='Mailing State/Province']/following::input[@name='province'][1]")).sendKeys("TamilNadu");
        driver.findElement(By.xpath("//label[text()='Mailing Zip/Postal Code']/following::input[@name='postalCode'][1]")).sendKeys("600127");
        driver.findElement(By.xpath("//label[text()='Mailing Country']/following::input[@name='country'][1]")).sendKeys("INDIA");
        
        //Update level as tertiary
        WebElement ter =driver.findElement(By.xpath("//label[text()='Level']/following::button/span[text()='--None--']"));
        js.executeScript("arguments[0].scrollIntoView();", ter);	
        ter.click();
        driver.findElement(By.xpath("//span[@title='Tertiary']")).click();
       
        //Update Title as Automation Testing
        WebElement title = driver.findElement(By.xpath("//input[@name='Title']"));
        js.executeScript("arguments[0].scrollIntoView();", title);	
        title.sendKeys("Automation Testing");
      
        //Click save and verify and print Email
        driver.findElement(By.xpath("//button[@name='SaveEdit']")).click();
        Thread.sleep(5000);
        String emailid = driver.findElement(By.xpath("//a[@class='emailuiFormattedEmail']")).getText();
        System.out.println(emailid); 
        if(emailid.equals(upatedemail)) {
        	System.out.println("=====Email id is updated as expected====");
        }
        else {
        	System.out.println(">>>>>>>Email id is not updated<<<<<<<");
        }

       
	}

}
