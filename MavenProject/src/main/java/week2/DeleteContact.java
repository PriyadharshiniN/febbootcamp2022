package week2;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DeleteContact {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		driver.get("https://login.salesforce.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofMillis(6000));
        
		//Login to the application
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.xpath("//input[@id = 'password']")).sendKeys("BootcampSel$123");
        driver.findElement(By.xpath("//input[@id ='Login']")).click();
        
        //Click on Global Actions SVG icon
        driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
        driver.findElement(By.xpath("//button[text()='View All']")).click();
        
        //Scroll down till the element is found
        JavascriptExecutor js = (JavascriptExecutor) driver;
        WebElement contact  = driver.findElement(By.xpath("//p[text()='Contacts']"));
        js.executeScript("arguments[0].scrollIntoView();", contact);
        
        //Click on contacts
        contact.click();
        
        // Get the size of contacts available and print the list
        List<WebElement> rowcount = driver.findElements(By.xpath("//table[@class='slds-table forceRecordLayout slds-table--header-fixed slds-table--edit slds-table--bordered resizable-cols slds-table--resizable-cols uiVirtualDataTable']/tbody/tr"));
        System.out.println("Total number of contacts created :=>"+rowcount.size());
        
        //Search the contact using unique name
        driver.findElement(By.xpath("//input[@placeholder='Search this list...']")).sendKeys("MuraliSS");
        driver.findElement(By.xpath("//input[@placeholder='Search this list...']")).sendKeys(Keys.ENTER);
        Thread.sleep(5000);
        driver.findElement(By.xpath("//table/tbody/tr/th/span/a[contains(text(), Icumed)]")).click();
        
        // Get the text of Contact name and store it 
        @SuppressWarnings("deprecation")
		WebDriverWait wait = new WebDriverWait(driver,5);
 	    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@class='custom-truncate uiOutputText']")));
        String ContactName = driver.findElement(By.xpath("//span[@class='custom-truncate uiOutputText']")).getText();
        System.out.println("contact nam is"+ ContactName);
       
	    
        //Click on the dropdown icon available in the unique contact and select edit
        driver.findElement(By.xpath("//button[@class='slds-button slds-button_icon-border-filled']")).click();
        driver.findElement(By.xpath("//span[text()='Delete']")).click();
        driver.findElement(By.xpath("//button[@title='Delete']")).click();
        
        //Verify whether the Contact is Deleted 
        driver.findElement(By.xpath("//input[@placeholder='Search this list...']")).clear();
        driver.findElement(By.xpath("//input[@placeholder='Search this list...']")).sendKeys("MuraliSS");
        driver.findElement(By.xpath("//input[@placeholder='Search this list...']")).sendKeys(Keys.ENTER);
        boolean contdel = driver.findElement(By.xpath("//span[text()='No items to display.']")).isDisplayed();
        String result = contdel ? "Contact is deleted" :"Contact is not  deleted";
        System.out.println(result);
        
        

	}

}
