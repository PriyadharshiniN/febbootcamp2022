package week3;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class DeleteServiceTerritory {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		driver.get("https://login.salesforce.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofMillis(6000));
        
		//Login to the application
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.xpath("//input[@id = 'password']")).sendKeys("BootcampSel$123");
        driver.findElement(By.xpath("//input[@id ='Login']")).click();
        
        //Click on View All
        driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
        driver.findElement(By.xpath("//button[text()='View All']")).click();
        
        //Click on Service Territories
        JavascriptExecutor js = (JavascriptExecutor) driver;
        WebElement serviceTerritory  = driver.findElement(By.xpath("//p[text()='Service Territories']"));
        js.executeScript("arguments[0].scrollIntoView();", serviceTerritory);
        serviceTerritory.click();
        
        //Under Service Territories on Left Corner Click the Dropdown Option
        driver.findElement(By.xpath("//span[text()='Recently Viewed']")).click();
        
        //Select All ServiceTerritories
        driver.findElement(By.xpath("//span[text()='All Service Territories']")).click();
        String createdname = "PriyaMurali";
        
        // Select The Newly created Service territory ,Click on the Dropdown Icon on right Corner
        List<WebElement> rowcount = driver.findElements(By.xpath("//div[@class='uiVirtualDataTable indicator']/following::table[1]/tbody/tr"));
        int i; 
        System.out.println(rowcount.size());
        for( i =1; i<=rowcount.size(); i++) {
          String chkname =	driver.findElement(By.xpath("//div[@class='uiVirtualDataTable indicator']/following::table[1]/tbody/tr["+i+"]/th/span/a")).getText();
           if(chkname.equals(createdname)) {
        	   driver.findElement(By.xpath("//div[@class='uiVirtualDataTable indicator']/following::table[1]/tbody/tr["+i+"]/td[8]")).click();
        	   break;
           }
        }
        
        //Click on delete
        driver.findElement(By.xpath("//a[@title='Delete']")).click();
        
        //Click on Delete  on Confirmation box
        driver.findElement(By.xpath("//span[text()='Delete']")).click();
       
        //Verify Whether the Service Territory is deleted Successfully
        List<WebElement> newrowcount = driver.findElements(By.xpath("//div[@class='uiVirtualDataTable indicator']/following::table[1]/tbody/tr"));
        int j; 
        boolean b=false;
        System.out.println(newrowcount.size());
        for( j =1; j<=newrowcount.size(); j++) {
          String chkname =	driver.findElement(By.xpath("//div[@class='uiVirtualDataTable indicator']/following::table[1]/tbody/tr["+j+"]/th/span/a")).getText();
           if(chkname.equals(createdname)) {
              b = false;
           }
           else {
        	   b = true;
        	   
           }
        }
        System.out.println(b);
        String terrdel = (b) ? "Territory deleted": "Territory not deleted";
        System.out.println(terrdel);
	}

}
