package week3;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class CreateParentterritory {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		driver.get("https://login.salesforce.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofMillis(6000));
        
		//Login to the application
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.xpath("//input[@id = 'password']")).sendKeys("BootcampSel$123");
        driver.findElement(By.xpath("//input[@id ='Login']")).click();
        
        //Click on View All
        driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
        driver.findElement(By.xpath("//button[text()='View All']")).click();
        
        //Click on Service Territories
        WebElement serviceTerritory  = driver.findElement(By.xpath("//p[text()='Service Territories']"));
        js.executeScript("arguments[0].scrollIntoView();", serviceTerritory);
        serviceTerritory.click();
        
        //Click on Parent Territory Input field of newly created Service Territory
        String titlename = "Priyadharshini Neelakandan";
 
        List<WebElement> rowcount = driver.findElements(By.xpath("//div[@class='uiVirtualDataTable indicator']/following::table[1]/tbody/tr"));
        for(int i =1; i<=rowcount.size(); i++ ) {
        	String gettext = driver.findElement(By.xpath("//div[@class='uiVirtualDataTable indicator']/following::table[1]/tbody/tr["+i+"]/th/span/a")).getText();
        	if (gettext.equals(titlename)) {
        		WebElement clickedit = driver.findElement(By.xpath("//div[@class='uiVirtualDataTable indicator']/following::table[1]/tbody/tr["+i+"]/td[5]/span/span[2]/button"));
        		js.executeScript("arguments[0].click();", clickedit);
        		break;
        	}
        }
        
        //Under that Click on New Service Territory
        driver.findElement(By.xpath("//span[@title='New Service Territory']")).click();
        
        //Enter Name as Mukesh Ambani
        driver.findElement(By.xpath("//span[text()='Parent Territory']/preceding::input[@class=' input']")).sendKeys("Mukesh Ambani");
	    
        //Click on Search Operating Hours
        driver.findElement(By.xpath("//input[@title='Search Operating Hours']")).click();
        
        //Click New Operating Hours in the DropDown
        driver.findElement(By.xpath("//span[@title='New Operating Hours']")).click();
        
        String opnameSent = "Mukesh Ambani";
        //Enter Name as Mukesh Ambani
        driver.findElement(By.xpath("//h2[text()='New Operating Hours']/following::input[@class=' input']")).sendKeys(opnameSent);
        
        // Select Time as India standard Time(Asia/Kolkata)
        driver.findElement(By.xpath("//a[@class='select']")).click();
        driver.findElement(By.xpath("//div[@class='select-options']/ul/li[2]")).click();
 
        // Click on Save
        driver.findElement(By.xpath("//h2[text()='New Operating Hours']/following::button[@title='Save']")).click();
        
        
        //Verify the name for operating hours
        String ophourName = driver.findElement(By.xpath("//span[@class='pillText']")).getText();
        String ophourverify = (ophourName.equals(opnameSent))? "Verified the op hour name": "Name not verified";
        System.out.println(ophourverify);
        //Click on save
        driver.findElement(By.xpath("//button[@title='Save']")).click();
        
        //Verify Whether Parent Territory is Created Successfully
        String getparentterr = "";
        List<WebElement> rowcount2 = driver.findElements(By.xpath("//div[@class='uiVirtualDataTable indicator']/following::table[1]/tbody/tr"));
        for(int i =1; i<=rowcount2.size(); i++ ) {
        	String gettext = driver.findElement(By.xpath("//div[@class='uiVirtualDataTable indicator']/following::table[1]/tbody/tr["+i+"]/th/span/a")).getText();
        	if (gettext.equals(titlename)) {
        		getparentterr = driver.findElement(By.xpath("//div[@class='uiVirtualDataTable indicator']/following::table[1]/tbody/tr["+i+"]/td[5]/span/a")).getText();
        		break;
        	}
        }
	
	    String verification =(getparentterr.equals(opnameSent))? "Parent Territory addded successfully": "Parent territory is not addded";
	    System.out.println(verification);
	
	}

}
