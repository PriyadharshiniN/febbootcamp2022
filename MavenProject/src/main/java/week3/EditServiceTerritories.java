package week3;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class EditServiceTerritories {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		  WebDriverManager.chromedriver().setup();
		  ChromeOptions options = new ChromeOptions();
		  options.addArguments("--disable-notifications");
		  ChromeDriver driver = new ChromeDriver(options);
		  driver.get("https://login.salesforce.com");
		  driver.manage().window().maximize();
		  driver.manage().timeouts().implicitlyWait(Duration.ofMillis(6000));
        
		//Login to the application
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.xpath("//input[@id = 'password']")).sendKeys("BootcampSel$123");
        driver.findElement(By.xpath("//input[@id ='Login']")).click();
        
        //Click on Global Actions SVG icon
        driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
        driver.findElement(By.xpath("//button[text()='View All']")).click();
        
        //Scroll down till the element is found
        JavascriptExecutor js = (JavascriptExecutor) driver;
        WebElement serviceTerritory  = driver.findElement(By.xpath("//p[text()='Service Territories']"));
        js.executeScript("arguments[0].scrollIntoView();", serviceTerritory);
        serviceTerritory.click();
        
        //Click on Down Arrow DropDown and choose edit
        driver.findElement(By.xpath("//input[@name='ServiceTerritory-search-input']")).sendKeys("PriyaMurali");
        driver.findElement(By.xpath("//input[@name='ServiceTerritory-search-input']")).sendKeys(Keys.ENTER);
        
        //Click on Down Arrow DropDown and choose edit
        Thread.sleep(5000);
        driver.findElement(By.xpath("//table/tbody/tr/td[8][@class='slds-cell-edit cellContainer']")).click();
        driver.findElement(By.xpath("//div[@class='branding-actions actionMenu popupTargetContainer uiPopupTarget uiMenuList forceActionsDropDownMenuList uiMenuList--left uiMenuList--default visible positioned']/div/ul/li[1]/a[@title='Edit']")).click();        
        
        //Get the System Information Created by Using Regex print the name alone
        Actions a = new Actions(driver);
        a.sendKeys(Keys.PAGE_DOWN).build().perform();
        String createdBy = driver.findElement(By.xpath("//h2[@class='inlineTitle slds-p-top--large slds-p-horizontal--medium slds-p-bottom--medium slds-text-heading--medium']/following::span[@class='uiOutputText forceOutputLookup'][2]")).getText();
        System.out.println("Created By:=>"+ createdBy );
	    
        // Get the System Information Modified By Using Regex print the name alone
        String modifiedBy = driver.findElement(By.xpath("//h2[@class='inlineTitle slds-p-top--large slds-p-horizontal--medium slds-p-bottom--medium slds-text-heading--medium']/following::span[@class='uiOutputText forceOutputLookup'][3]")).getText();
        System.out.println("Modified By:=>"+ modifiedBy );
        
        //Get the text of the owner 
        String owner = driver.findElement(By.xpath("//h2[@class='inlineTitle slds-p-top--large slds-p-horizontal--medium slds-p-bottom--medium slds-text-heading--medium']/following::span[@class='uiOutputText forceOutputLookup'][3]")).getText();
        System.out.println("Owner Name:=>"+ owner );
        
        // Verify Owner, Created By and Modified By are matching
        String match = (createdBy.equals(modifiedBy) && modifiedBy.equals(owner))? "All 3 are matching" : "Not Matching";
        System.out.println(match);
        
        //Change the country name to North America
        driver.findElement(By.xpath("//input[@placeholder='Country']")).clear();
        driver.findElement(By.xpath("//input[@placeholder='Country']")).sendKeys("North America");
        
        // Click on Save
        driver.findElement(By.xpath("//button[@title='Save']")).click();
        
        //Verify LastModified date
        SimpleDateFormat formatter = new SimpleDateFormat("M/d/yyyy");  
        Date date = new Date(); 
        String sysdate = formatter.format(date);
        System.out.println(sysdate);  
        String modDate = driver.findElement(By.xpath("//table/tbody/tr[1]/td[6]/span/span[@class='slds-truncate uiOutputDateTime']")).getText();
        System.out.println(modDate); 
        String [] dates =modDate.split(",");
        System.out.println(dates[0]);
        String dateverify = (sysdate.equals(dates[0]))? "Last Modified date verified":"Last Modified date differs";
        System.out.println(dateverify);
	}

}
