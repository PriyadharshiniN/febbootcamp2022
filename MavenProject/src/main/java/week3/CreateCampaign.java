package week3;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class CreateCampaign extends Login {
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		driver.get("https://login.salesforce.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofMillis(6000));
    	Login lg = new Login();
		
	    JavascriptExecutor js = (JavascriptExecutor) driver;
        
		//Login to the application
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.xpath("//input[@id = 'password']")).sendKeys("BootcampSel$123");
		driver.findElement(By.xpath("//input[@id ='Login']")).click();
		
		//Click on view all
//		  lg.viewAll();
        driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
        driver.findElement(By.xpath("//button[text()='View All']")).click();
//        
        //Click on Sales
        driver.findElement(By.xpath("//p[@title='Manage your sales process with accounts, leads, opportunities, and more']")).click();
        
        // Click on Campaigns tab
        WebElement campaigntab  = driver.findElement(By.xpath("//a[@title='Campaigns']"));
        js.executeScript("arguments[0].click();", campaigntab);
        
        
        String clickname = "Bootcamp";
      
        //Click Bootcamp link
         List<WebElement> rows = driver.findElements(By.xpath("//table[@aria-label='Recently Viewed']/tbody/tr"));
        for(int i=1; i <= rows.size(); i++) {
        String titlename = 	driver.findElement(By.xpath("//table[@aria-label='Recently Viewed']/tbody/tr["+i+"]//th/span/a")).getText();
        if (titlename.equals(clickname)) {
        	driver.findElement(By.xpath("//table[@aria-label='Recently Viewed']/tbody/tr["+i+"]//th/span/a")).click();
        }
        }
        
        //Click on New Contact
        WebElement contact = driver.findElement(By.xpath("//div[@title='New Contact']"));
        js.executeScript("arguments[0].click();", contact);
        
        //Pick Salutation as 'Mr.'
        String salutation = "Mr.";
        driver.findElement(By.xpath("//div[@class='salutation compoundTLRadius compoundTRRadius compoundBorderBottom form-element__row uiMenu']")).click();
        driver.findElement(By.xpath("//li/a[@title='"+salutation+"']")).click();
        
        //Enter first name as <your First Name>
        String firstName = "Priyas_campaign";
        driver.findElement(By.xpath("//input[@placeholder='First Name']")).sendKeys(firstName);
        
        //Enter last name as <your last name>
        String lastName = "test3";
        driver.findElement(By.xpath("//input[@placeholder='Last Name']")).sendKeys(lastName);
        
        //Click on Save
        driver.findElement(By.xpath("//div[@class='modal-footer slds-modal__footer']/child::button[2]")).click();
        
        //Verify the toast message
        String firstmsg =driver.findElement(By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']")).getText();
        System.out.println(firstmsg);
        
        //Click on Add Contact from Campaign member
        driver.findElement(By.xpath("//div[@class='forceRelatedListContainer']/div/child::div[4]/article/div[1]/div/div/ul/li/a/div[@title='Add Contacts']")).click();
        
        
	}

}
