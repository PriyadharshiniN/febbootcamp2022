package week3;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.github.bonigarcia.wdm.WebDriverManager;

public class FirstAssessment_Priyadharshini {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		//Webdriver Setup
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofMillis(6000));
		
		//Login to https://login.salesforce.com
		driver.get("https://login.salesforce.com");
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.xpath("//input[@id = 'password']")).sendKeys("BootcampSel$123");
        driver.findElement(By.xpath("//input[@id ='Login']")).click();
		
		//Click on toggle menu button from the left corner
        driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
        
        // Click view All 
        driver.findElement(By.xpath("//button[text()='View All']")).click();
        
        //Click Service Console from App Launcher
        driver.findElement(By.xpath("//p[text()='Service Console']")).click();
        
        //Select Home from the DropDown
        Thread.sleep(5000);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        WebElement navMenu = driver.findElement(By.xpath("//span[text()='Show Navigation Menu']"));
        js.executeScript("arguments[0].click();", navMenu);
        driver.findElement(By.xpath("//a[@data-label='Home']")).click();
        
        //Select Dashboards from DropDown
        Thread.sleep(5000);
        js.executeScript("arguments[0].click();", navMenu);
        driver.findElement(By.xpath("//a[@data-label='Dashboards']")).click();
        
        //Click on New Dashboard
        driver.findElement(By.xpath("//div[@title='New Dashboard']")).click();
        
        //Enter the Dashboard name as "YourName_Workout"
        List<WebElement> frames = driver.findElements(By.tagName("iframe"));
		for(int i=0; i<frames.size();i++)
		{
			if(frames.get(i).getAttribute("title").equalsIgnoreCase("Dashboard"))
			{
				Thread.sleep(7000);
				driver.switchTo().frame(i);
			}
		}
		Thread.sleep(5000);
		@SuppressWarnings("deprecation")
		WebDriverWait wait = new WebDriverWait(driver,7);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='dashboardNameInput']")));
		WebElement name = driver.findElement(By.xpath("//input[@id='dashboardNameInput']"));
		String nameSent = "PriyaN_workout";
		name.sendKeys(nameSent);
       
		// Enter Description as Testing and Click on Create
		driver.findElement(By.xpath("//input[@id='dashboardDescriptionInput']")).sendKeys("Testing");
        driver.findElement(By.xpath("//button[text()='Create']")).click();
        driver.switchTo().defaultContent();
        Thread.sleep(5000);
        driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='dashboard']")));
       
        //Click on Done
        driver.findElement(By.xpath("//button[text()='Done']")).click();
        
       //Verify the Dashboard is Created
        boolean dashboardText   = driver.findElement(By.xpath("//span[text()='Dashboard']")).isDisplayed();
        String dashboardName    = driver.findElement(By.xpath("//span[@class='slds-page-header__title slds-truncate']")).getText();
        String dashboardCreated = (dashboardName.equals(nameSent) && dashboardText == true ) ? "Dashboard Created Successfully": "Daashboard not created";
        System.out.println(dashboardCreated);
       
       // Click on Subscribe 	
        driver.findElement(By.xpath("//div[@class='slds-dropdown-trigger slds-dropdown-trigger_click slds-button_last']/preceding::button[text()='Subscribe']")).click();
        driver.switchTo().defaultContent();
        
       // Select Frequency as "Daily
        driver.findElement(By.xpath("//span[text()='Daily']")).click();
        
       //Time as 10:00 AM
        WebElement time = driver.findElement(By.id("time"));
        Select selectime = new Select(time);
        selectime.selectByVisibleText("10:00 AM");
       
       // Click on Save
        driver.findElement(By.xpath("//span[text()='Save']")).click();
        
        //Verify "You started Dashboard Subscription" message displayed or not
        String expMsg      = "You started a dashboard subscription.";
        String subscrMsg   = driver.findElement(By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']")).getText();
        String msgVerified = (subscrMsg.equals(expMsg))? "Message Verified Successfully!!!":"Message is not verified";
        System.out.println(msgVerified);
       
       //Close the "YourName_Workout" tab
        driver.switchTo().defaultContent();
        driver.findElement(By.xpath("//div[@class='selectedListItem slds-col--bump-left slds-p-left--x-small slds-context-bar__item slds-context-bar__object-switcher ']/a")).click();
        
        //Click on Private Dashboards
        driver.findElement(By.xpath("//h2[text()='Dashboards']/following::ul[1]/li/a[text()='Private Dashboards']")).click();
        
        //Verify the newly created Dashboard available
        driver.findElement(By.xpath("//input[@placeholder='Search private dashboards...']")).sendKeys(nameSent);
        driver.findElement(By.xpath("//input[@placeholder='Search private dashboards...']")).sendKeys(Keys.ENTER);
        String checkdbname    = driver.findElement(By.xpath("//table[@class='slds-table slds-table_header-fixed slds-table_bordered slds-table_edit slds-table_resizable-cols']/tbody/tr/th/lightning-primitive-cell-factory/span/div/lightning-formatted-url/a")).getText();
        String dbnameVerified = (checkdbname.equals(nameSent))? "Newly Created dashboard available!!!!!":">>>>Newly Created dashboard not  available<<<<<<<";
        System.out.println(dbnameVerified);
        
       // Click on dropdown for the item
       wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//table[@class='slds-table slds-table_header-fixed slds-table_bordered slds-table_edit slds-table_resizable-cols']/tbody/tr/td[6]")));
       WebElement dropdown = driver.findElement(By.xpath("//table[@class='slds-table slds-table_header-fixed slds-table_bordered slds-table_edit slds-table_resizable-cols']/tbody/tr/td[6]"));
       js.executeScript("arguments[0].scrollIntoView();", dropdown);
       dropdown.click();
       
       //Click on delete
       WebElement delete = driver.findElement(By.xpath("//lightning-button-menu[@class='slds-dropdown-trigger slds-dropdown-trigger_click slds-is-open']/div/div/slot/lightning-menu-item/a/span[text()='Delete']"));
       js.executeScript("arguments[0].click();", delete);
       driver.findElement(By.xpath("//button[@title='Delete']")).click();
       String deletemsg   = driver.findElement(By.xpath("//span[@class='toastMessage slds-text-heading--small forceActionsText']")).getText();
       String deletetoast = (deletemsg.contains("deleted"))? "Dashboard Deleted verifed in toast message":"unable to verify Dashboard Deleted verification in toast message ";
	   System.out.println(deletetoast);
	   
	   //Verify the item is not available under Private Dashboard folder
	   driver.findElement(By.xpath("//input[@placeholder='Search private dashboards...']")).clear();
       driver.findElement(By.xpath("//input[@placeholder='Search private dashboards...']")).sendKeys(nameSent);
       driver.findElement(By.xpath("//input[@placeholder='Search private dashboards...']")).sendKeys(Keys.ENTER);	
       boolean result = driver.findElement(By.className("emptyMessageTitle")).isDisplayed();
       String deleteVerify = result? "Dashboard deleted verified":"dahsboard not deleted";
       System.out.println(deleteVerify);
	   
	}

}
