package week3;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class CreateServiceTerriotry {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--disable-notifications");
		ChromeDriver driver = new ChromeDriver(options);
		driver.get("https://login.salesforce.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofMillis(6000));
        
		//Login to the application
		driver.findElement(By.id("username")).sendKeys("makaia@testleaf.com");
		driver.findElement(By.xpath("//input[@id = 'password']")).sendKeys("BootcampSel$123");
        driver.findElement(By.xpath("//input[@id ='Login']")).click();
        
        //Click on Global Actions SVG icon
        driver.findElement(By.xpath("//div[@class='slds-icon-waffle']")).click();
        driver.findElement(By.xpath("//button[text()='View All']")).click();
        
        //Scroll down till the element is found
        JavascriptExecutor js = (JavascriptExecutor) driver;
        WebElement serviceTerritory  = driver.findElement(By.xpath("//p[text()='Service Territories']"));
        js.executeScript("arguments[0].scrollIntoView();", serviceTerritory);
        serviceTerritory.click();
        
        //Click on New 
        driver.findElement(By.xpath("//li[@class='slds-button slds-button--neutral']/a/div[text()='New']")).click();
        
        //Enter Your Name in Name field
        driver.findElement(By.xpath("//span[text()='Parent Territory']/preceding::input[@class=' input']")).sendKeys("PriyaMurali");
	    
        //Click on Operating Hours and Choose the First option
        driver.findElement(By.xpath("//input[@title='Search Operating Hours']")).click();
        driver.findElement(By.xpath("//span[@title='New Operating Hours']")).click();
        driver.findElement(By.xpath("//h2[text()='New Operating Hours']/following::input[@class=' input']")).sendKeys("New OP Hour");
        driver.findElement(By.xpath("//a[@class='select']")).click();
        driver.findElement(By.xpath("//div[@class='select-options']/ul/li[2]")).click();
        driver.findElement(By.xpath("//h2[text()='New Operating Hours']/following::button[@title='Save']")).click();
        
        //Check Active Field
        WebElement click = driver.findElement(By.xpath("//h2[text()='New Service Territory']/following::input[@type='checkbox']"));
        js.executeScript("arguments[0].click();", click);
        
        //Enter the City your residing in City Field
        driver.findElement(By.xpath("//input[@placeholder='City']")).sendKeys("Chennai");
        
        // Enter the State your residing in State Field
        driver.findElement(By.xpath("//input[@placeholder='State/Province']")).sendKeys("TamilNadu");
        
        //Enter the Country your residing in Country Field
        Actions a = new Actions(driver);
        a.sendKeys(Keys.PAGE_DOWN).build().perform();
        Thread.sleep(5000);
        WebElement country = driver.findElement(By.xpath("//input[@placeholder='Country']"));
        country.sendKeys("INDIA");
        
        //Enter your current Postal Zip Code
        driver.findElement(By.xpath("//input[@placeholder='Zip/Postal Code']")).sendKeys("600129");
        
        //Click on Save
        driver.findElement(By.xpath("//button[@title='Save']")).click();
        
        //Verify the service territory message
        String message = driver.findElement(By.xpath("//div[@class='slds-theme--success slds-notify--toast slds-notify slds-notify--toast forceToastMessage']")).getText();
        String result = message.contains("Service Territory \"PriyaMurali\" was created.")? "service territory message verified successfully": "Unable to verify service territory message";
        System.out.println(message);
        System.out.println(result);
	}

}
